# coding:utf-8
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY = 's=pxxkq)s_7g=k=ecj#*8$#%f=3r25oboffqm-v2*mfdnim5#4'
DEBUG = True
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = ['artlessons.ru']
INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'gallery',
    'collector',
    #'sysmon',
)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
ROOT_URLCONF = 'artlessons.urls'
WSGI_APPLICATION = 'artlessons.wsgi.application'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'artlessons_database.sqlite3'),
    }
}
LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
MEDIA_ROOT = os.path.join(os.path.expanduser('~'), 'domains/artlessons.ru/media')
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(os.path.expanduser('~'), 'domains/artlessons.ru/static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'statics'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
