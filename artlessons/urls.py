# coding:utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.shortcuts import redirect
import settings
from collector.views import login, collector, tryers_page
admin.autodiscover()
# перемещаем пользователя с домена
def redirect_to_gallery(request):
    return redirect('/gallery')

urlpatterns = patterns('',
    url(r'^artlessons_dash/', include(admin.site.urls)),
    url(r'^$', 'gallery.views.works'),
    url(r'^admin/$', login),
    url(r'^admin.php/$', login),
    url(r'^administrator/$', login),
    url(r'^administrator.php/$', login),
    url(r'^wp-admin/$', login),
    url(r'^wp-admin.php/$', login),
    url(r'^login/$', login),
    url(r'^login.php/$', login),
    url(r'^thanks/$', collector),
    url(r'^tryings/$', tryers_page),
    url(r'^gallery/', include('gallery.urls')),


)
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True }),
    )
