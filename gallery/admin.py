from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Category)
admin.site.register(MaksimichWork)
admin.site.register(SavostikovaWork)
admin.site.register(Student)
admin.site.register(StudentWork)
admin.site.register(Technicks)
