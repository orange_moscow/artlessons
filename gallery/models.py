# coding:utf-8
from django.db import models

# Create your models here.
class Category(models.Model):
    class Meta:
        db_table = u'categories'
        verbose_name = u'категория'
        verbose_name_plural = u'категории'

    name = models.CharField(verbose_name=u'имя', max_length=50)

    def __unicode__(self):
        return self.name

class Technicks(models.Model):
    class Meta:
        db_table = u'technicks'
        verbose_name = u'техника'
        verbose_name_plural = u'техники'

    name = models.CharField(verbose_name=u'имя',max_length=20)

    def __unicode__(self):
        return self.name


class MaksimichWork(models.Model):
    class Meta:
        db_table = u'maximich_works'
        verbose_name = u'работа Максимыча'
        verbose_name_plural = u'работы Максимыча'

    name = models.CharField(verbose_name=u'имя', max_length=50)
    size = models.CharField(verbose_name=u'размер', max_length=50, blank=True)
    category = models.ForeignKey(Category)
    technicks = models.ForeignKey(Technicks)
    thumbnail = models.ImageField(verbose_name=u'превью', upload_to='media/thumb')
    image = models.ImageField(verbose_name=u'полноформатное изображение', upload_to='media/full')
    year = models.CharField(verbose_name=u'год',max_length=4)
    position = models.IntegerField(verbose_name=u'Позиция', default=0)

    def __unicode__(self):
        return self.name





class SavostikovaWork(models.Model):
    class Meta:
        db_table = u'savostikova_works'
        verbose_name = u'работа Савостиковой'
        verbose_name_plural = u'работы Савостиковой'

    name = models.CharField(verbose_name=u'имя', max_length=50)
    size = models.CharField(verbose_name=u'размер', max_length=50, blank=True)
    technicks = models.ForeignKey(Technicks)
    thumbnail = models.ImageField(verbose_name=u'превью', upload_to='media/thumb')
    image = models.ImageField(verbose_name=u'полноформатное изображение', upload_to='media/full')
    year = models.CharField(verbose_name=u'год',max_length=4)
    position = models.IntegerField(verbose_name=u'Позиция', default=0)
    

    def __unicode__(self):
        return self.name


class Student(models.Model):
    class Meta:
        db_table = u'students'
        verbose_name = u'ученик'
        verbose_name_plural = u'ученики'

    name = models.CharField(verbose_name=u'имя', max_length=50)

    def __unicode__(self):
        return self.name


class StudentWork(models.Model):
    class Meta:
        db_table = u'students_works'
        verbose_name = u'работа ученика'
        verbose_name_plural = u'работы учеников'

    name = models.CharField(verbose_name=u'имя', max_length=30)
    size = models.CharField(verbose_name=u'размер', max_length=50, blank=True)
    author = models.ForeignKey(Student, verbose_name=u'автор')
    technicks = models.ForeignKey(Technicks)
    thumbnail = models.ImageField(verbose_name=u'превью', upload_to='media/thumb')
    image = models.ImageField(verbose_name=u'полноформатное изображение', upload_to='media/full')
    year = models.CharField(verbose_name=u'год',max_length=4)
    position = models.IntegerField(verbose_name=u'Позиция', default=0)

    def __unicode__(self):
        return self.name
