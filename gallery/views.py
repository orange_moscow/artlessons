# coding: utf-8
from django.shortcuts import render_to_response
from .models import *
from artlessons.settings import MEDIA_URL


def works(request):
    return render_to_response(u'gallery/works.html')

def index_page(request):
    students = Student.objects.all()
    categories = Category.objects.all()
    return render_to_response(u'gallery/index.html', locals())


def work_by_cat(request, cat_id):
    media = MEDIA_URL
    students = Student.objects.all()
    categories = Category.objects.all()
    work_category = Category.objects.get(pk=cat_id)
    link_categories = '/gallery/{}/'.format(work_category.id)
    title = work_category.name
    works = MaksimichWork.objects.filter(category=work_category).order_by('position')
    return render_to_response(u'gallery/w_b_k.html', locals())


def one_work(request, good_id):
    categories = Category.objects.all()
    students = Student.objects.all()
    work = MaksimichWork.objects.get(pk=good_id)
    return render_to_response(u'gallery/one_work.html', locals())

def base(request):
    students = Student.objects.all()
    categories = Category.objects.all()
    return render_to_response(u'gallery/base.html', locals())

def contacts(request):
    students = Student.objects.all()
    link_contacts = u'/gallery/contacts/'
    categories = Category.objects.all()
    return render_to_response(u'gallery/contacts.html', locals())

def savostikova(request):
    students = Student.objects.all()
    link_savostikova = '/gallery/savostikova/'
    categories = Category.objects.all()
    return render_to_response(u'gallery/savostikova.html',locals())

def savostikova_works(request):
    students = Student.objects.all()
    link_savostikova_works = '/gallery/savostikova_works/'
    works = SavostikovaWork.objects.all().order_by('position')
    categories = Category.objects.all()
    return render_to_response(u'gallery/savosticova_works.html',locals())

def one_savosticova_work(request, work_id):
    students = Student.objects.all()
    categories = Category.objects.all()
    work = SavostikovaWork.objects.get(pk=work_id)
    return render_to_response(u'gallery/one_work.html', locals())

def students_work(request, student_id):
    students = Student.objects.all()
    categories = Category.objects.all()
    work_student = Student.objects.get(pk=student_id)
    title = work_student.name
    works = StudentWork.objects.filter(author=work_student).order_by('position')
    return render_to_response(u'gallery/students_works.html', locals())

def one_student_work(request, work_id):
    students = Student.objects.all()
    categories = Category.objects.all()
    work = StudentWork.objects.get(pk=work_id)
    return render_to_response(u'gallery/one_work.html', locals())

def education(request):
    students = Student.objects.all()
    categories = Category.objects.all()
    return render_to_response('gallery/edu.html',locals())




