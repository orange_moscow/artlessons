# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u0438\u043c\u044f')),
            ],
            options={
                'db_table': 'categories',
                'verbose_name': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MaksimichWork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u0438\u043c\u044f')),
                ('size', models.CharField(max_length=50, verbose_name='\u0440\u0430\u0437\u043c\u0435\u0440')),
                ('thumbnail', models.ImageField(upload_to=b'media/thumb', verbose_name='\u043f\u0440\u0435\u0432\u044c\u044e')),
                ('image', models.ImageField(upload_to=b'media/full', verbose_name='\u043f\u043e\u043b\u043d\u043e\u0444\u043e\u0440\u043c\u0430\u0442\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('year', models.CharField(max_length=4, verbose_name='\u0433\u043e\u0434')),
                ('position', models.IntegerField()),
                ('category', models.ForeignKey(to='gallery.Category')),
            ],
            options={
                'db_table': 'maximich_works',
                'verbose_name': '\u0440\u0430\u0431\u043e\u0442\u0430 \u041c\u0430\u043a\u0441\u0438\u043c\u044b\u0447\u0430',
                'verbose_name_plural': '\u0440\u0430\u0431\u043e\u0442\u044b \u041c\u0430\u043a\u0441\u0438\u043c\u044b\u0447\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SavostikovaWork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u0438\u043c\u044f')),
                ('size', models.CharField(max_length=50, verbose_name='\u0440\u0430\u0437\u043c\u0435\u0440')),
                ('thumbnail', models.ImageField(upload_to=b'media/thumb', verbose_name='\u043f\u0440\u0435\u0432\u044c\u044e')),
                ('image', models.ImageField(upload_to=b'media/full', verbose_name='\u043f\u043e\u043b\u043d\u043e\u0444\u043e\u0440\u043c\u0430\u0442\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('year', models.CharField(max_length=4, verbose_name='\u0433\u043e\u0434')),
                ('position', models.IntegerField()),
            ],
            options={
                'db_table': 'savostikova_works',
                'verbose_name': '\u0440\u0430\u0431\u043e\u0442\u0430 \u0421\u0430\u0432\u043e\u0441\u0442\u0438\u043a\u043e\u0432\u043e\u0439',
                'verbose_name_plural': '\u0440\u0430\u0431\u043e\u0442\u044b \u0421\u0430\u0432\u043e\u0441\u0442\u0438\u043a\u043e\u0432\u043e\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u0438\u043c\u044f')),
            ],
            options={
                'db_table': 'students',
                'verbose_name': '\u0443\u0447\u0435\u043d\u0438\u043a',
                'verbose_name_plural': '\u0443\u0447\u0435\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StudentWork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='\u0438\u043c\u044f')),
                ('size', models.CharField(max_length=50, verbose_name='\u0440\u0430\u0437\u043c\u0435\u0440')),
                ('thumbnail', models.ImageField(upload_to=b'media/thumb', verbose_name='\u043f\u0440\u0435\u0432\u044c\u044e')),
                ('image', models.ImageField(upload_to=b'media/full', verbose_name='\u043f\u043e\u043b\u043d\u043e\u0444\u043e\u0440\u043c\u0430\u0442\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('year', models.CharField(max_length=4, verbose_name='\u0433\u043e\u0434')),
                ('position', models.IntegerField()),
                ('author', models.ForeignKey(verbose_name='\u0430\u0432\u0442\u043e\u0440', to='gallery.Student')),
            ],
            options={
                'db_table': 'students_works',
                'verbose_name': '\u0440\u0430\u0431\u043e\u0442\u0430 \u0443\u0447\u0435\u043d\u0438\u043a\u0430',
                'verbose_name_plural': '\u0440\u0430\u0431\u043e\u0442\u044b \u0443\u0447\u0435\u043d\u0438\u043a\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Technicks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, verbose_name='\u0438\u043c\u044f')),
            ],
            options={
                'db_table': 'technicks',
                'verbose_name': '\u0442\u0435\u0445\u043d\u0438\u043a\u0430',
                'verbose_name_plural': '\u0442\u0435\u0445\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='studentwork',
            name='technicks',
            field=models.ForeignKey(to='gallery.Technicks'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='savostikovawork',
            name='technicks',
            field=models.ForeignKey(to='gallery.Technicks'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='maksimichwork',
            name='technicks',
            field=models.ForeignKey(to='gallery.Technicks'),
            preserve_default=True,
        ),
    ]
