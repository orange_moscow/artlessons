# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0004_auto_20150124_1932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maksimichwork',
            name='size',
            field=models.CharField(max_length=50, verbose_name='\u0440\u0430\u0437\u043c\u0435\u0440', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='savostikovawork',
            name='size',
            field=models.CharField(max_length=50, verbose_name='\u0440\u0430\u0437\u043c\u0435\u0440', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='studentwork',
            name='size',
            field=models.CharField(max_length=50, verbose_name='\u0440\u0430\u0437\u043c\u0435\u0440', blank=True),
            preserve_default=True,
        ),
    ]
