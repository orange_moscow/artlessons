# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0003_auto_20150124_1928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maksimichwork',
            name='position',
            field=models.IntegerField(default=0, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='savostikovawork',
            name='position',
            field=models.IntegerField(default=0, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='studentwork',
            name='position',
            field=models.IntegerField(default=0, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
            preserve_default=True,
        ),
    ]
