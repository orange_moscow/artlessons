# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0002_auto_20150124_1926'),
    ]

    operations = [
        migrations.AddField(
            model_name='savostikovawork',
            name='position',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='studentwork',
            name='position',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='maksimichwork',
            name='position',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
    ]
