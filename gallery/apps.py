#coding: utf-8
from django.apps import AppConfig

class Gallery(AppConfig):
    name = 'gallery'
    verbose_name = u'Галерея'
