# coding: utf-8
from django.conf.urls import patterns, url
from .views import *
urlpatterns = patterns('',
                       url(r'^$', index_page, name='index'),
                       url(r'^base/$', base),
                       url(r'^education/$', education, name='education'),
                       url(r'^savostikova/$', savostikova,name='savostikova'),
                       url(r'^savostikova_works/$', savostikova_works, name='savostikova_works'),
                       url(r'^savostikova_works/([a-zA-Z0-9]+)/$', one_savosticova_work, name='one_savosticova_work'),
                       url(r'^contacts/$', contacts, name=u'contacts'),
                       url(r'^students/([a-zA-Z0-9]+)/$', students_work),
                       url(r'^students/works/([a-zA-Z0-9]+)/$', one_student_work),
                       url(r'^([a-zA-Z0-9]+)/$', work_by_cat),
                       url(r'^works/([a-zA-Z0-9]+)/$', one_work),
)
